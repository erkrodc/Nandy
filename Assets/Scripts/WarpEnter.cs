﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WarpEnter : MonoBehaviour {

	public string SceneToLoad;
	public string ExitIdentifier;

	private PlayerController player;

	void Start(){
		player = FindObjectOfType<PlayerController>();
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.name == "Player"){
			SceneManager.LoadScene(SceneToLoad);
			player.startPoint = ExitIdentifier;
		}
	}
}
