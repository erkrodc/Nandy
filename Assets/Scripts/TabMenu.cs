﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabMenu : MonoBehaviour {

	public CanvasGroup TabMenuCanvasGroup;
	public GameObject InventoryPanel;
	public GameObject SkillsPanel;
	public GameObject PowersPanel;
	public GameObject GearPanel;
	public GameObject MenuPanel;

	private Dictionary<string, GameObject> PanelDict;
	private bool IsActive;

	// Use this for initialization
	void Start () {
		//create dict for panels
		PanelDict = new Dictionary<string, GameObject>();
		PanelDict.Add("InventoryPanel", InventoryPanel);
		PanelDict.Add("SkillsPanel", SkillsPanel);
		PanelDict.Add("PowersPanel", PowersPanel);
		PanelDict.Add("GearPanel", GearPanel);
		PanelDict.Add("MenuPanel", MenuPanel);

		IsActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		//make menu visible/interactable depending on "IsActive" value
		if (IsActive){
			TabMenuCanvasGroup.alpha = 1;
			TabMenuCanvasGroup.interactable = true;
			PanelsSetActive(true);
		}
		else{
			TabMenuCanvasGroup.alpha = 0;
			TabMenuCanvasGroup.interactable = false;
			PanelsSetActive(false);
		}

		//toggle "IsActive" when tab key is pressed
		if (Input.GetKeyDown(KeyCode.Tab))
			IsActive = !IsActive;
	}

	private void PanelsSetActive(bool active){
		foreach (KeyValuePair<string, GameObject> KVP in PanelDict){
			KVP.Value.SetActive(active);
		}
	}
}
