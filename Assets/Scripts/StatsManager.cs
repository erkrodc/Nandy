﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PixelCrushers.DialogueSystem;

public class StatsManager : MonoBehaviour {

	private static bool managerExists;
	private static Dictionary<string, int> levels;

	// Use this for initialization
	void Start () {
		if (managerExists){
			//since manager exists, delete the instance of the manager that this run of this script is creating
			Destroy(gameObject);
		}else{
			//set to true since we're in the script that is creating the manager instance.
			managerExists = true;
			levels = new Dictionary<string, int>();
			DontDestroyOnLoad(gameObject);
		}
	}

	/// <summary>
	/// iterate over each stat,
	/// check to see if it exists in the database (might be able to skip this step if only using one database with every possible skill existing as a variable in the database),
	/// and if it does exist, sync its value with that found in "levels" dictionary
	/// </summary>
	public void SyncVarsWithDialogueDB(){
		foreach (KeyValuePair<string, int> pair in levels){
			if (DialogueLua.DoesVariableExist(pair.Key)){
				DialogueLua.SetVariable(pair.Key, pair.Value);
			}
		}
	}

	public int GetLevel(string skill){
		return levels[skill];
	}

	public void SetLevel(string skill, int value){
		if (!levels.ContainsKey(skill)) return;
		levels[skill] = value;
	}

	public void SetLevel(string KeyValuePair){
		string Key = KeyValuePair.Split(',')[0];
		int Value = Int32.Parse(KeyValuePair.Split(',')[1]);
		SetLevel(Key, Value);
	}

	public void AddSkill(string skill, int value){
		if (levels.ContainsKey(skill)) return;
		levels.Add(skill, value);
	}

	public void AddSkill(string KeyValuePair){
		string Key = KeyValuePair.Split(',')[0];
		int Value = Int32.Parse(KeyValuePair.Split(',')[1]);
		AddSkill(Key, Value);
	}
}
