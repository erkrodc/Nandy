﻿using UnityEngine;
using System.Collections;

public class PlayerController: MonoBehaviour {

	//set to static so that duplicates of players know to delete themselves because another instance exists
	private static bool playerExists;
	private Rigidbody2D rbody;
	private Animator anim;
	private float speed;

	public string startPoint;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();

		if (playerExists){
			//since player exists, delete the instance of the player that this run of this script is creating
			Destroy(gameObject);
		}else{
			//set to true since we're in the script that is creating the player GO.
			playerExists = true;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 movement_vector = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		speed = !Input.GetKey(KeyCode.LeftShift) ? 1.2f : 0.5f;

		if (movement_vector != Vector2.zero){
			anim.SetBool("iswalking", true);
			anim.SetFloat("input_x", movement_vector.x);
			anim.SetFloat("input_y", movement_vector.y);
		}else{
			anim.SetBool("iswalking", false);
		}

		rbody.position = (rbody.position + movement_vector * speed * Time.deltaTime);

	}
}