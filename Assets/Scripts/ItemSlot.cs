﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Nandy.Item;

public class ItemSlot : MonoBehaviour, IDropHandler {
	public int slotID;
	private Inventory inv;

	void Start(){
		inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
	}

	public void OnDrop(PointerEventData eventData){
		ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();

		if (inv.items[slotID].ID == -1){
			inv.items[droppedItem.slotIndex] = new Item();
			inv.items[slotID] = droppedItem.item;
			droppedItem.slotIndex = slotID;
		}else{
			Transform item = this.transform.GetChild(0);
			item.GetComponent<ItemData>().slotIndex = droppedItem.slotIndex;
			item.SetParent(inv.slots[droppedItem.slotIndex].transform);
			item.transform.position = inv.slots[droppedItem.slotIndex].transform.position;

			/*droppedItem.slotIndex = slotID;
			droppedItem.transform.SetParent(this.transform);
			droppedItem.transform.position = this.transform.position;*/

			inv.items[droppedItem.slotIndex] = item.GetComponent<ItemData>().item;
			inv.items[slotID] = droppedItem.item;
			droppedItem.slotIndex = slotID;
		}
	}
}
