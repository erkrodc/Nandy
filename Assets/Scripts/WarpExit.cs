﻿using UnityEngine;
using System.Collections;

public class WarpExit : MonoBehaviour {

	private PlayerController player;
	private CameraController mainCam;

	public string exitIdentifier;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
		mainCam = FindObjectOfType<CameraController>();

		if (player.startPoint == exitIdentifier){
			player.transform.position = transform.position;
			mainCam.transform.position = new Vector3(transform.position.x, transform.position.y, mainCam.transform.position.z);
		}
	}
}
