﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EditStats")]
public class EditStats : ScriptableObject {

	StatsManager statMan;

	void OnEnable(){
		statMan = FindObjectOfType<StatsManager>();
	}

	/// <summary>
	/// iterate over each stat,
	/// check to see if it exists in the database (might be able to skip this step if only using one database with every possible skill existing as a variable in the database),
	/// and if it does exist, sync its value with that found in "levels" dictionary
	/// </summary>
	public void SyncStats(){
		statMan.SyncVarsWithDialogueDB();
	}

	public void SetLevel(string KVPair){
		statMan.SetLevel(KVPair);
	}

	public void AddSkill(string KVPair){
		statMan.AddSkill(KVPair);
	}
}
