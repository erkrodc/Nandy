﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using Nandy.Item;
using Nandy.Stats;

public class ItemsDatabase : MonoBehaviour {
	private List<Item> Database = new List<Item>();
	private List<Weapon> WeaponsDB = new List<Weapon>();
	private List<Armor> ArmorsDB = new List<Armor>();
	private List<Consumable> ConsumablesDB = new List<Consumable>();
	private List<Ammunition> AmmunitionsDB = new List<Ammunition>();
	private List<QuestItem> QuestItemsDB = new List<QuestItem>();
	private JsonData ItemData;

	void Start(){
		ItemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json"));
		ConstructItemDatabase();
	}

	private void ConstructItemDatabase(){

		Dictionary<StatType, Scale> scales = new Dictionary<StatType, Scale>();
		Dictionary<StatType, int> prerequisites = new Dictionary<StatType, int>();
		StatType stat = StatType.None;
		Scale scale = Scale.None;
		for (int i=0; i<ItemData.Count; i++){
			scales.Clear();
			prerequisites.Clear();
			
			int id = (int)ItemData[i]["ID"];
			int value = (int)ItemData[i]["Value"];
			string title = ItemData[i]["Title"].ToString();
			string description = ItemData[i]["Description"].ToString();
			string imagePath = ItemData[i]["ImagePath"].ToString();
			string type = ItemData[i]["Type"].ToString();

			JsonData scaleJSON;
			JsonData prereqJSON;
			switch(type){
				case "Weapon":
					scaleJSON = ItemData[i]["Scales"];
					prereqJSON = ItemData[i]["Prerequisites"];

					foreach (string key in scaleJSON.Keys){
						switch(key){
							case "Sympathy":
								stat = StatType.Sympathy;
								break;
							case "Intelligence":
								stat = StatType.Intelligence;
								break;
							case "Pride":
								stat = StatType.Pride;
								break;
							case "Apathy":
								stat = StatType.Apathy;
								break;
							case "Mischief":
								stat = StatType.Mischief;
								break;
							case "Strength":
								stat = StatType.Strength;
								break;
							default:
								break;
						}
						
						switch(scaleJSON[key].ToString()){
							case "S":
								scale = Scale.S;
								break;
							case "A":
								scale = Scale.A;
								break;
							case "B":
								scale = Scale.B;
								break;
							case "C":
								scale = Scale.C;
								break;
							case "N":
								scale = Scale.N;
								break;
							default:
								break;
						}
						scales.Add(stat, scale);
					}

					foreach (string key in prereqJSON.Keys){
						switch(key){
						case "Sympathy":
							stat = StatType.Sympathy;
							break;
						case "Intelligence":
							stat = StatType.Intelligence;
							break;
						case "Pride":
							stat = StatType.Pride;
							break;
						case "Apathy":
							stat = StatType.Apathy;
							break;
						case "Mischief":
							stat = StatType.Mischief;
							break;
						case "Strength":
							stat = StatType.Strength;
							break;
						default:
							break;
						}

						prerequisites.Add(stat, (int)prereqJSON[key]);
					}

					Weapon weapon = new Weapon(	id, value, (int)ItemData[i]["Weight"], title, description, imagePath,
												(int)ItemData[i]["Rarity"],	(int)ItemData[i]["MeleeDamage"], (int)ItemData[i]["MagicDamage"], scales, prerequisites);
					WeaponsDB.Add(weapon);
					Database.Add(weapon);
					break;
				case "Consumable":
					Consumable consumable = new Consumable(	id, value, title, description, imagePath,
															(int)ItemData[i]["HealthRegen"], (int)ItemData[i]["MagicRegen"]);
					ConsumablesDB.Add(consumable);
					Database.Add(consumable);
					break;
				case "Armor":
					scaleJSON = ItemData[i]["Scales"];
					prereqJSON = ItemData[i]["Prerequisites"];
					foreach (string key in scaleJSON.Keys){
						switch(key){
						case "sympathy":
							stat = StatType.Sympathy;
							break;
						case "intelligence":
							stat = StatType.Intelligence;
							break;
						case "pride":
							stat = StatType.Pride;
							break;
						case "apathy":
							stat = StatType.Apathy;
							break;
						case "mischief":
							stat = StatType.Mischief;
							break;
						case "strength":
							stat = StatType.Strength;
							break;
						default:
							break;
						}

						switch(scaleJSON[key].ToString()){
						case "S":
							scale = Scale.S;
							break;
						case "A":
							scale = Scale.A;
							break;
						case "B":
							scale = Scale.B;
							break;
						case "C":
							scale = Scale.C;
							break;
						case "N":
							scale = Scale.N;
							break;
						default:
							break;
						}
						scales.Add(stat, scale);
					}

					foreach (string key in prereqJSON.Keys){
						switch(key){
						case "sympathy":
							stat = StatType.Sympathy;
							break;
						case "intelligence":
							stat = StatType.Intelligence;
							break;
						case "pride":
							stat = StatType.Pride;
							break;
						case "apathy":
							stat = StatType.Apathy;
							break;
						case "mischief":
							stat = StatType.Mischief;
							break;
						case "strength":
							stat = StatType.Strength;
							break;
						default:
							break;
						}

						prerequisites.Add(stat, (int)prereqJSON[key]);
					}

					Armor armor = new Armor(	id, value, (int)ItemData[i]["Weight"], title, description, imagePath,
												(int)ItemData[i]["Rarity"], (int)ItemData[i]["MeleeDefense"], (int)ItemData[i]["MagicDefense"], scales, prerequisites);
					ArmorsDB.Add(armor);
					Database.Add(armor);
					break;
				case "Ammunition":
					Ammunition ammunition = new Ammunition(	id, value, title, description, imagePath,
															(int)ItemData[i]["MeleeDamage"], (int)ItemData[i]["MagicDamage"]);
					AmmunitionsDB.Add(ammunition);
					Database.Add(ammunition);
					break;
				case "QuestItem":
					QuestItem questItem = new QuestItem(id, title,  description, imagePath, (bool)ItemData[i]["Wearable"], (int)ItemData[i]["QuestID"]);

					QuestItemsDB.Add(questItem);
					Database.Add(questItem);
					break;
				default:
					break;
			}
		}
	}

	public Item GetItemByID(int id){
		foreach (Item item in Database){
			if (item.ID == id) return item;
		}

		return null;
	}

	public Weapon GetWeaponByID(int id){
		foreach (Weapon weapon in WeaponsDB){
			if (weapon.ID == id) return weapon;
		}

		return null;
	}

	public Armor GetArmorByID(int id){
		foreach (Armor armor in ArmorsDB){
			if (armor.ID == id) return armor;
		}

		return null;
	}

	public Consumable GetConsumableByID(int id){
		foreach (Consumable consumable in ConsumablesDB){
			if (consumable.ID == id) return consumable;
		}

		return null;
	}

	public Ammunition GetAmmunitionByID(int id){
		foreach (Ammunition ammunition in AmmunitionsDB){
			if (ammunition.ID == id) return ammunition;
		}

		return null;
	}

	public QuestItem GetQuestItemByID(int id){
		foreach (QuestItem questItem in QuestItemsDB){
			if (questItem.ID == id) return questItem;
		}

		return null;
	}
}