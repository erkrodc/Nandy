﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nandy.Stats {
	public enum StatType{
		Sympathy,
		Intelligence,
		Pride,
		Apathy,
		Mischief,
		Strength,
		None //Used for initializing
	}

	public enum Scale{
		S,
		A,
		B,
		C,
		N,
		None //Used for initializing
	}
}
