﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nandy.Stats;

namespace Nandy.Item {
	public enum ItemType{
		Weapon,
		Consumable,
		Armor,
		Ammunition,
		QuestItem
	}

	public class Item {
		public int ID {get; set;}
		public int Value {get; set;}
		public int Weight {get; set;}
		public string Title {get; set;}
		public string Description {get; set;}
		public string ImagePath {get; set;}
		public bool Stackable {get; set;}
		public bool Wearable {get; set;}
		public ItemType Type {get; set;}
		public Sprite Sprite {get; set;}

		public Item(){
			this.ID = -1;
		}

		public Item(int id, int value, int weight, string title, string description, string imagePath, bool stackable, bool wearable, ItemType type){
			this.ID = id;
			this.Value = value;
			this.Weight = weight;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Stackable = stackable;
			this.Wearable = wearable;
			this.Type = type;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);
		}
	}

	public class Weapon : Item {
		public int Rarity {get; set;}
		public int MeleeDamage {get; set;}
		public int MagicDamage {get; set;}
		public Dictionary<StatType, Scale> Scales;
		public Dictionary<StatType, int> Prerequisites;

		public Weapon(	int id, int value, int weight, string title, string description, string imagePath,
			int rarity, int meleeDamage, int magicDamage, Dictionary<StatType, Scale> scales, Dictionary<StatType, int> prerequisites){

			this.Stackable = false;
			this.Wearable = true;
			this.Type = ItemType.Weapon;

			this.ID = id;
			this.Value = value;
			this.Weight = weight;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);

			this.Rarity = rarity;
			this.MeleeDamage = meleeDamage;
			this.MagicDamage = magicDamage;
			this.Scales = new Dictionary<StatType, Scale>(scales);
			this.Prerequisites = new Dictionary<StatType, int>(prerequisites);
		}
	}

	public class Armor : Item {
		public int Rarity {get; set;}
		public int MeleeDefense {get; set;}
		public int MagicDefense {get; set;}
		public Dictionary<StatType, Scale> Scales;
		public Dictionary<StatType, int> Prerequisites;

		public Armor(	int id, int value, int weight, string title, string description, string imagePath,
			int rarity, int meleeDefense, int magicDefense, Dictionary<StatType, Scale> scales, Dictionary<StatType, int> prerequisites){

			this.Stackable = false;
			this.Wearable = true;
			this.Type = ItemType.Armor;

			this.ID = id;
			this.Value = value;
			this.Weight = weight;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);

			this.Rarity = rarity;
			this.MeleeDefense = meleeDefense;
			this.MagicDefense = magicDefense;
			this.Scales = new Dictionary<StatType, Scale>(scales);
			this.Prerequisites = new Dictionary<StatType, int>(prerequisites);
		}
	}

	public class Consumable : Item {
		public int HealthRegen {get; set;}
		public int MagicRegen{get; set;}

		public Consumable(	int id, int value, string title, string description, string imagePath,
			int healthRegen, int magicRegen){

			this.Weight = 0;
			this.Stackable = true;
			this.Wearable = false;
			this.Type = ItemType.Consumable;

			this.ID = id;
			this.Value = value;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);

			this.HealthRegen = healthRegen;
			this.MagicRegen = magicRegen;
		}
	}

	public class Ammunition : Item {
		public int MeleeDamage {get; set;}
		public int MagicDamage {get; set;}

		public Ammunition(	int id, int value, string title, string description, string imagePath,
			int meleeDamage, int magicDamage){

			this.Weight = 0;
			this.Stackable = true;
			this.Wearable = true;
			this.Type = ItemType.Ammunition;

			this.ID = id;
			this.Value = value;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);

			this.MeleeDamage = meleeDamage;
			this.MagicDamage = magicDamage;

		}
	}

	public class QuestItem : Item {
		public int QuestID {get; set;}

		public QuestItem(int id, string title, string description, string imagePath, bool wearable, int questID){
			this.Value = 0;
			this.Weight = 0;
			this.Stackable = false;
			this.Type = ItemType.QuestItem;

			this.ID = id;
			this.Title = title;
			this.Description = description;
			this.ImagePath = imagePath;
			this.Sprite = Resources.Load<Sprite>(this.ImagePath);
			this.Wearable = wearable;

			this.QuestID = questID;
		}
	}
}