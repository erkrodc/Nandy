﻿using UnityEngine;
using System.Collections;

public class CameraController: MonoBehaviour {

	public Transform target;
	public float Follow_Speed = 0.1f;
	Camera cam;

	private static bool cameraExists;
	
	
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();

		if (cameraExists){
			//since player exists, delete the instance of the player that this run of this script is creating
			Destroy(gameObject);
		}else{
			//set to true since we're in the script that is creating the player GO.
			cameraExists = true;
			DontDestroyOnLoad(gameObject);
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		cam.orthographicSize = (Screen.height / 100f) / 3f;
		
		if(target){
			transform.position = Vector3.Lerp(transform.position, target.position, Follow_Speed) + new Vector3(0,0,-10);
		}
	}
}
