﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Nandy.Item;

public class ItemData : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler {

	public Item item;
	public int amount;
	public int slotIndex;

	private Vector2 offset;
	private Inventory inv;

	void Start(){
		inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
	}

	public void OnPointerDown(PointerEventData eventData){
		if (item != null){
			offset = eventData.position - (Vector2)this.transform.position;
			//detach children from parent slot for this item???
			this.transform.SetParent(this.transform.parent.parent);
			this.transform.position = eventData.position - offset;
			GetComponent<CanvasGroup>().blocksRaycasts = false;
		}
	}

	public void OnDrag(PointerEventData eventData){
		if (item != null){
			this.transform.position = eventData.position - offset;
		}
	}

	public void OnPointerUp(PointerEventData eventData){
		this.transform.SetParent(inv.slots[slotIndex].transform);
		this.transform.position = inv.slots[slotIndex].transform.position;
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

}