﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nandy.Item;

public class Inventory : MonoBehaviour {
	public GameObject ItemsContent;
	public GameObject SlotPrefab;
	public GameObject ItemPrefab;
	public ItemsDatabase itemsDB;
	public int InventorySize = 35;

	public List<GameObject> slots = new List<GameObject>();
	public List<Item> items = new List<Item>();

	void Start(){
		LoadSavedInventory();
		for (int i=0; i<InventorySize; i++){
			items.Add(new Item());
			slots.Add(Instantiate(SlotPrefab));
			slots[i].GetComponent<ItemSlot>().slotID = i;
			slots[i].transform.SetParent(ItemsContent.transform);
		}

		//render each item
		for (int i=0; i<20; i++)
			AddItem(2);

		for (int i=0; i<20; i++){
			RemoveItem(2);
		}

		AddItem(2);
		AddItem(2);
		AddItem(2);

		RemoveStackable(2);

		AddItem(2);
		AddItem(2);
	}

	private void LoadSavedInventory(){
		//pull saved inventory information from json file? and load it into "InventoryList", namely inventorysize and items list
	}

	public void AddItem(int ItemID){
		Item item = itemsDB.GetItemByID(ItemID);
		if (item.Stackable){
			int inventoryIndex = GetItemIndex(ItemID);
			if (inventoryIndex != -1){
				ItemData data = slots[inventoryIndex].transform.GetChild(0).GetComponent<ItemData>();

				data.amount++;
				data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();

				return;
			}
		}

		for (int i=0; i<items.Count; i++){
			if (items[i].ID == -1){
				items[i] = item;

				GameObject itemObj = Instantiate(ItemPrefab);

				ItemData itemObjData = itemObj.GetComponent<ItemData>();
				itemObjData.item = item;
				itemObjData.slotIndex = i;

				itemObj.name = item.Title;
				itemObj.transform.SetParent(slots[i].transform);
				itemObj.GetComponent<Image>().sprite = item.Sprite;
				itemObj.transform.position = Vector2.zero;
				if (item.Stackable) slots[i].transform.GetChild(0).GetComponent<ItemData>().amount = 1;
				return;
			}
		}
	}

	//Separated from RemoveItem(int) for efficiency (prevent unnecessary calls to GetItemIndex)
	//This is the main logic for removing an item
	private void RemoveItem(int ItemID, int InventoryIndex){

		//handle all cases of removing stackable item: data.amount >= 2 (decrement data.amount and update number), and data.amount == 1 (destroy gameObject)
		if (items[InventoryIndex].Stackable){
			ItemData data = slots[InventoryIndex].transform.GetChild(0).GetComponent<ItemData>();
			if (data.amount > 1){
				data.amount--;
				if (data.amount > 1)
					data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();
				else
					data.transform.GetChild(0).GetComponent<Text>().text = "";
			}
			else{
				items[InventoryIndex] = new Item();
				Destroy(slots[InventoryIndex].transform.GetChild(0).gameObject);
				slots[InventoryIndex].transform.DetachChildren();
			}
		}else{ //branch for non-stackable items, simply destroy gameObject (and unparent from slot so as to keep slots[InventoryIndex].ChildCount = 1 for future placement of items)
			items[InventoryIndex] = new Item();
			Destroy(slots[InventoryIndex].transform.GetChild(0).gameObject);
			slots[InventoryIndex].transform.DetachChildren();
		}
	}
		
	/// <summary>
	/// Removes first item with ID == ItemID.
	/// </summary>
	/// <param name="ItemID">Item I.</param>
	public void RemoveItem(int ItemID){
		int index = GetItemIndex(ItemID);
		if (index == -1) return;
		RemoveItem(ItemID, index);
	}

	/// <summary>
	/// Removes all items in a single stack whose ID == ItemID.
	/// </summary>
	public void RemoveStackable(int ItemID){
		int index = GetItemIndex(ItemID);
		if (index == -1) return;
		slots[index].transform.GetChild(0).GetComponent<ItemData>().amount = 1;
		RemoveItem(ItemID, index);
	}
		
	/// <summary>
	/// Find the index for the first item whose ID matches "ItemID". Returns -1 if none is found.
	/// </summary>
	private int GetItemIndex(int ItemID){
		for (int i=0; i < items.Count; i++){
			if (items[i].ID == ItemID){
				return i;
			}
		}
		return -1;
	}
}